PK    x��X             	 pen-export-RwmXPgZ/README.mdUT e�f# 

A Pen created on CodePen.io. Original URL: [https://codepen.io/Pratik09/pen/RwmXPgZ](https://codepen.io/Pratik09/pen/RwmXPgZ).

PK�viy�   �   PK    x��X             	 pen-export-RwmXPgZ/LICENSE.txtUT e�fThe MIT License (MIT)

Copyright (c) 2024 Pratik (https://codepen.io/Pratik09/pen/RwmXPgZ)
Fork of an original work  (https://codepen.io/Pratik09/pen/RwmXPgZ)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
PK�ZAٟ  �  PK    x��X            ! 	 pen-export-RwmXPgZ/src/index.htmlUT e�f<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-commerce App</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>My E-commerce Store</h1>
        <nav id="nav" style="display: none;">
            <a href="#" onclick="showHomePage()">Home</a>
            <a href="#" onclick="showCartPage()">Cart</a>
            <a href="#" onclick="showOrderStatusPage()">Order Status</a>
            <a href="#" onclick="logout()">Logout</a>
        </nav>
    </header>
    <main id="content">
        <!-- Dynamic content will be loaded here -->
    </main>
    <script src="script.js"></script>
</body>
</html>PKEؘ��  �  PK    x��X              	 pen-export-RwmXPgZ/src/style.cssUT e�fbody {
    font-family: Arial, sans-serif;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

header {
    background-color: #333;
    color: white;
    padding: 1rem;
    text-align: center;
}

nav a {
    color: white;
    margin: 0 10px;
    text-decoration: none;
}

main {
    padding: 1rem;
}

.product-list, .cart-items, .order-status {
    display: flex;
    flex-wrap: wrap;
}

.product, .cart-item, .order {
    border: 1px solid #ccc;
    border-radius: 5px;
    padding: 1rem;
    margin: 0.5rem;
    text-align: center;
    width: 30%;
}

button {
    background-color: #333;
    color: white;
    border: none;
    padding: 0.5rem 1rem;
    cursor: pointer;
    border-radius: 5px;
}

button:hover {
    background-color: #555;
}PK����  �  PK    x��X              	 pen-export-RwmXPgZ/src/script.jsUT e�fconst products = [
    { id: 1, name: 'Product 1', price: 100 },
    { id: 2, name: 'Product 2', price: 200 },
    { id: 3, name: 'Product 3', price: 300 }
];

const cart = [];
const orders = [];
const users = [];
let loggedIn = false;

document.addEventListener('DOMContentLoaded', () => {
    showLoginPage();
});

function showLoginPage() {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Login</h2>
        <form onsubmit="login(event)">
            <label for="username">Username:</label><br>
            <input type="text" id="username" name="username" required><br>
            <label for="password">Password:</label><br>
            <input type="password" id="password" name="password" required><br><br>
            <input type="submit" value="Login">
        </form>
        <p>Don't have an account? <a href="#" onclick="showRegisterPage()">Register</a></p>
    `;
}

function showRegisterPage() {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Register</h2>
        <form onsubmit="register(event)">
            <label for="new-username">Username:</label><br>
            <input type="text" id="new-username" name="new-username" required><br>
            <label for="new-password">Password:</label><br>
            <input type="password" id="new-password" name="new-password" required><br><br>
            <input type="submit" value="Register">
        </form>
        <p>Already have an account? <a href="#" onclick="showLoginPage()">Login</a></p>
    `;
}

function register(event) {
    event.preventDefault();
    const newUsername = document.getElementById('new-username').value;
    const newPassword = document.getElementById('new-password').value;

    if (users.find(user => user.username === newUsername)) {
        alert('Username already exists');
    } else {
        users.push({ username: newUsername, password: newPassword });
        alert('Registration successful. Please log in.');
        showLoginPage();
    }
}

function login(event) {
    event.preventDefault();
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    const user = users.find(user => user.username === username && user.password === password);
    if (user) {
        loggedIn = true;
        document.getElementById('nav').style.display = 'block';
        showHomePage();
    } else {
        alert('Invalid username or password');
    }
}

function logout() {
    loggedIn = false;
    document.getElementById('nav').style.display = 'none';
    showLoginPage();
}

function showHomePage() {
    if (!loggedIn) {
        alert('Please login first');
        return;
    }
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Products</h2>
        <div class="product-list" id="product-list"></div>
    `;
    displayProducts();
}

function showCartPage() {
    if (!loggedIn) {
        alert('Please login first');
        return;
    }
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Shopping Cart</h2>
        <div class="cart-items" id="cart-items"></div>
        <button onclick="showCheckoutPage()">Checkout</button>
    `;
    displayCart();
}

function showCheckoutPage() {
    if (!loggedIn) {
        alert('Please login first');
        return;
    }
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Checkout</h2>
        <form onsubmit="placeOrder(event)">
            <label for="name">Name:</label><br>
            <input type="text" id="name" name="name" required><br>
            <label for="address">Address:</label><br>
            <input type="text" id="address" name="address" required><br><br>
            <input type="submit" value="Place Order">
        </form>
    `;
}

function showOrderStatusPage() {
    if (!loggedIn) {
        alert('Please login first');
        return;
    }
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Order Status</h2>
        <div class="order-status" id="order-status"></div>
    `;
    displayOrders();
}

function displayProducts() {
    const productList = document.getElementById('product-list');
    productList.innerHTML = '';

    products.forEach(product => {
        const productElement = document.createElement('div');
        productElement.classList.add('product');
        productElement.innerHTML = `
            <h3>${product.name}</h3>
            <p>Price: $${product.price}</p>
            <button onclick="addToCart(${product.id})">Add to Cart</button>
        `;
        productList.appendChild(productElement);
    });
}

function addToCart(productId) {
    const product = products.find(p => p.id === productId);
    cart.push(product);
    alert(`${product.name} added to cart`);
}

function displayCart() {
    const cartItems = document.getElementById('cart-items');
    cartItems.innerHTML = '';

    cart.forEach((item, index) => {
        const cartItem = document.createElement('div');
        cartItem.classList.add('cart-item');
        cartItem.innerHTML = `
            <h3>${item.name}</h3>
            <p>Price: $${item.price}</p>
            <button onclick="removeFromCart(${index})">Remove</button>
        `;
        cartItems.appendChild(cartItem);
    });
}

function removeFromCart(index) {
    cart.splice(index, 1);
    displayCart();
}

function placeOrder(event) {
    event.preventDefault();
    const name = document.getElementById('name').value;
    const address = document.getElementById('address').value;

    orders.push({ name, address, items: [...cart] });
    cart.length = 0;
    alert('Order placed successfully');
    showHomePage();
}

function displayOrders() {
    const orderStatus = document.getElementById('order-status');
    orderStatus.innerHTML = '';

    orders.forEach((order, index) => {
        const orderElement = document.createElement('div');
        orderElement.classList.add('order');
        orderElement.innerHTML = `
            <h3>Order ${index + 1}</h3>
            <p>Name: ${order.name}</p>
            <p>Address: ${order.address}</p>
            <p>Items:</p>
            <ul>
                ${order.items.map(item => `<li>${item.name} - $${item.price}</li>`).join('')}
            </ul>
        `;
        orderStatus.appendChild(orderElement);
    });
}PK8�s5  5  PK    x��X            " 	 pen-export-RwmXPgZ/dist/index.htmlUT e�f<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>CodePen - A Pen by Pratik</title>
  <link rel="stylesheet" href="./style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-commerce App</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>My E-commerce Store</h1>
        <nav id="nav" style="display: none;">
            <a href="#" onclick="showHomePage()">Home</a>
            <a href="#" onclick="showCartPage()">Cart</a>
            <a href="#" onclick="showOrderStatusPage()">Order Status</a>
            <a href="#" onclick="logout()">Logout</a>
        </nav>
    </header>
    <main id="content">
        <!-- Dynamic content will be loaded here -->
    </main>
    <script src="script.js"></script>
</body>
</html>
<!-- partial -->
  <script  src="./script.js"></script>

</body>
</html>
PK���    PK    x��X            ! 	 pen-export-RwmXPgZ/dist/style.cssUT e�fbody {
    font-family: Arial, sans-serif;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

header {
    background-color: #333;
    color: white;
    padding: 1rem;
    text-align: center;
}

nav a {
    color: white;
    margin: 0 10px;
    text-decoration: none;
}

main {
    padding: 1rem;
}

.product-list, .cart-items, .order-status {
    display: flex;
    flex-wrap: wrap;
}

.product, .cart-item, .order {
    border: 1px solid #ccc;
    border-radius: 5px;
    padding: 1rem;
    margin: 0.5rem;
    text-align: center;
    width: 30%;
}

button {
    background-color: #333;
    color: white;
    border: none;
    padding: 0.5rem 1rem;
    cursor: pointer;
    border-radius: 5px;
}

button:hover {
    background-color: #555;
}PK����  �  PK    x��X            ! 	 pen-export-RwmXPgZ/dist/script.jsUT e�fconst products = [
    { id: 1, name: 'Product 1', price: 100 },
    { id: 2, name: 'Product 2', price: 200 },
    { id: 3, name: 'Product 3', price: 300 }
];

const cart = [];
const orders = [];
const users = [];
let loggedIn = false;

document.addEventListener('DOMContentLoaded', () => {
    showLoginPage();
});

function showLoginPage() {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Login</h2>
        <form onsubmit="login(event)">
            <label for="username">Username:</label><br>
            <input type="text" id="username" name="username" required><br>
            <label for="password">Password:</label><br>
            <input type="password" id="password" name="password" required><br><br>
            <input type="submit" value="Login">
        </form>
        <p>Don't have an account? <a href="#" onclick="showRegisterPage()">Register</a></p>
    `;
}

function showRegisterPage() {
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Register</h2>
        <form onsubmit="register(event)">
            <label for="new-username">Username:</label><br>
            <input type="text" id="new-username" name="new-username" required><br>
            <label for="new-password">Password:</label><br>
            <input type="password" id="new-password" name="new-password" required><br><br>
            <input type="submit" value="Register">
        </form>
        <p>Already have an account? <a href="#" onclick="showLoginPage()">Login</a></p>
    `;
}

function register(event) {
    event.preventDefault();
    const newUsername = document.getElementById('new-username').value;
    const newPassword = document.getElementById('new-password').value;

    if (users.find(user => user.username === newUsername)) {
        alert('Username already exists');
    } else {
        users.push({ username: newUsername, password: newPassword });
        alert('Registration successful. Please log in.');
        showLoginPage();
    }
}

function login(event) {
    event.preventDefault();
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    const user = users.find(user => user.username === username && user.password === password);
    if (user) {
        loggedIn = true;
        document.getElementById('nav').style.display = 'block';
        showHomePage();
    } else {
        alert('Invalid username or password');
    }
}

function logout() {
    loggedIn = false;
    document.getElementById('nav').style.display = 'none';
    showLoginPage();
}

function showHomePage() {
    if (!loggedIn) {
        alert('Please login first');
        return;
    }
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Products</h2>
        <div class="product-list" id="product-list"></div>
    `;
    displayProducts();
}

function showCartPage() {
    if (!loggedIn) {
        alert('Please login first');
        return;
    }
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Shopping Cart</h2>
        <div class="cart-items" id="cart-items"></div>
        <button onclick="showCheckoutPage()">Checkout</button>
    `;
    displayCart();
}

function showCheckoutPage() {
    if (!loggedIn) {
        alert('Please login first');
        return;
    }
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Checkout</h2>
        <form onsubmit="placeOrder(event)">
            <label for="name">Name:</label><br>
            <input type="text" id="name" name="name" required><br>
            <label for="address">Address:</label><br>
            <input type="text" id="address" name="address" required><br><br>
            <input type="submit" value="Place Order">
        </form>
    `;
}

function showOrderStatusPage() {
    if (!loggedIn) {
        alert('Please login first');
        return;
    }
    const content = document.getElementById('content');
    content.innerHTML = `
        <h2>Order Status</h2>
        <div class="order-status" id="order-status"></div>
    `;
    displayOrders();
}

function displayProducts() {
    const productList = document.getElementById('product-list');
    productList.innerHTML = '';

    products.forEach(product => {
        const productElement = document.createElement('div');
        productElement.classList.add('product');
        productElement.innerHTML = `
            <h3>${product.name}</h3>
            <p>Price: $${product.price}</p>
            <button onclick="addToCart(${product.id})">Add to Cart</button>
        `;
        productList.appendChild(productElement);
    });
}

function addToCart(productId) {
    const product = products.find(p => p.id === productId);
    cart.push(product);
    alert(`${product.name} added to cart`);
}

function displayCart() {
    const cartItems = document.getElementById('cart-items');
    cartItems.innerHTML = '';

    cart.forEach((item, index) => {
        const cartItem = document.createElement('div');
        cartItem.classList.add('cart-item');
        cartItem.innerHTML = `
            <h3>${item.name}</h3>
            <p>Price: $${item.price}</p>
            <button onclick="removeFromCart(${index})">Remove</button>
        `;
        cartItems.appendChild(cartItem);
    });
}

function removeFromCart(index) {
    cart.splice(index, 1);
    displayCart();
}

function placeOrder(event) {
    event.preventDefault();
    const name = document.getElementById('name').value;
    const address = document.getElementById('address').value;

    orders.push({ name, address, items: [...cart] });
    cart.length = 0;
    alert('Order placed successfully');
    showHomePage();
}

function displayOrders() {
    const orderStatus = document.getElementById('order-status');
    orderStatus.innerHTML = '';

    orders.forEach((order, index) => {
        const orderElement = document.createElement('div');
        orderElement.classList.add('order');
        orderElement.innerHTML = `
            <h3>Order ${index + 1}</h3>
            <p>Name: ${order.name}</p>
            <p>Address: ${order.address}</p>
            <p>Items:</p>
            <ul>
                ${order.items.map(item => `<li>${item.name} - $${item.price}</li>`).join('')}
            </ul>
        `;
        orderStatus.appendChild(orderElement);
    });
}PK8�s5  5  PK    x��X�viy�   �    	         ��    pen-export-RwmXPgZ/README.mdUT e�fPK    x��X�ZAٟ  �   	         ���   pen-export-RwmXPgZ/LICENSE.txtUT e�fPK    x��XEؘ��  �  ! 	         ���  pen-export-RwmXPgZ/src/index.htmlUT e�fPK    x��X����  �    	         ��	  pen-export-RwmXPgZ/src/style.cssUT e�fPK    x��X8�s5  5    	         ��W  pen-export-RwmXPgZ/src/script.jsUT e�fPK    x��X���    " 	         ���%  pen-export-RwmXPgZ/dist/index.htmlUT e�fPK    x��X����  �  ! 	         ��?*  pen-export-RwmXPgZ/dist/style.cssUT e�fPK    x��X8�s5  5  ! 	         ���-  pen-export-RwmXPgZ/dist/script.jsUT e�fPK      �  G    